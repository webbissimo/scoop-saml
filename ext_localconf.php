<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Miniorange.ScoopMiniorange',
            'Fekey',
            [
                'Fesaml' => 'print'
            ],
            // non-cacheable actions
            [
                'Fesaml' => 'control'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Miniorange.ScoopMiniorange',
            'Responsekey',
            [
                'Response' => 'check'
            ],
            // non-cacheable actions
            [
                'Fesaml' => '',
                'Besaml' => '',
                'Response' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    fekey {
                        iconIdentifier = scoop_miniorange-plugin-fekey
                        title = LLL:EXT:scoop_miniorange/Resources/Private/Language/locallang_db.xlf:tx_ekey_fekey.name
                        description = LLL:EXT:scoop_miniorange/Resources/Private/Language/locallang_db.xlf:tx_ekey_fekey.description
                        tt_content_defValues {
                            CType = list
                            list_type = ekey_fekey
                        }
                    }
                    responsekey {
                        iconIdentifier = scoop_miniorange-plugin-responsekey
                        title = LLL:EXT:scoop_miniorange/Resources/Private/Language/locallang_db.xlf:tx_ekey_responsekey.name
                        description = LLL:EXT:scoop_miniorange/Resources/Private/Language/locallang_db.xlf:tx_ekey_responsekey.description
                        tt_content_defValues {
                            CType = list
                            list_type = ekey_responsekey
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'scoop_miniorange-plugin-fekey',
				\TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
				['source' => 'EXT:scoop_miniorange/Resources/Public/Icons/miniorange.png']
			);
		
			$iconRegistry->registerIcon(
				'scoop_miniorange-plugin-responsekey',
				\TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
				['source' => 'EXT:scoop_miniorange/Resources/Public/Icons/miniorange.png']
			);
		
    }
);
