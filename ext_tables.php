<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Miniorange.ScoopMiniorange',
            'Fekey',
            'fename'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Miniorange.ScoopMiniorange',
            'Responsekey',
            'response'
        );

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Miniorange.ScoopMiniorange',
                'tools', // Make module a submodule of 'tools'
                'bekey', // Submodule key
                '', // Position
                [
                    'Besaml' => 'request',
                    
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:scoop_miniorange/Resources/Public/Icons/miniorange.png',
                    'labels' => 'LLL:EXT:scoop_miniorange/Resources/Private/Language/locallang_bekey.xlf',
                ]
            );

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('scoop_miniorange', 'Configuration/TypoScript', 'etitle');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ekey_domain_model_fesaml', 'EXT:scoop_miniorange/Resources/Private/Language/locallang_csh_tx_ekey_domain_model_fesaml.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ekey_domain_model_fesaml');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ekey_domain_model_besaml', 'EXT:scoop_miniorange/Resources/Private/Language/locallang_csh_tx_ekey_domain_model_besaml.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ekey_domain_model_besaml');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ekey_domain_model_response', 'EXT:scoop_miniorange/Resources/Private/Language/locallang_csh_tx_ekey_domain_model_response.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ekey_domain_model_response');

    }
);
