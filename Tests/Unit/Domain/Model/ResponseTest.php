<?php
namespace Miniorange\ScoopMiniorange\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Miniorange <info@xecurify.com>
 */
class ResponseTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Miniorange\ScoopMiniorange\Domain\Model\Response
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Miniorange\ScoopMiniorange\Domain\Model\Response();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
